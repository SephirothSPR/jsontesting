package com.inoover.www.webapitesting;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;


import static com.inoover.www.webapitesting.Constants.FIRST_COLUMN;
import static com.inoover.www.webapitesting.Constants.SECOND_COLUMN;
import static com.inoover.www.webapitesting.Constants.THIRD_COLUMN;
import static com.inoover.www.webapitesting.Constants.FOURTH_COLUMN;

/**
 * Created by SephirothSPR on 10/17/2015.
 */
public class ListViewAdapters extends BaseAdapter {

    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    TextView txtUsrId;
    TextView txtId;
    TextView txtTitle;
    TextView txtBody;
    public ListViewAdapters(Activity activity, ArrayList <HashMap<String, String>> list){
        super();
        this.activity=activity;
        this.list=list;
    }

    public int getCount(){
        return list.size();
    }

    public Object getItem(int position){
        return list.get(position);
    }

    public long getItemId(int position){
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=activity.getLayoutInflater();

        if(convertView == null){

            convertView=inflater.inflate(R.layout.listviewcolumn, null);

            txtUsrId=(TextView) convertView.findViewById(R.id.USRID_CELL);
            txtId=(TextView) convertView.findViewById(R.id.ID_CELL);
            txtTitle=(TextView) convertView.findViewById(R.id.TITLE_CELL);
            txtBody=(TextView) convertView.findViewById(R.id.BODY_CELL);

        }

        HashMap<String, String> map=list.get(position);
        txtUsrId.setText(map.get(FIRST_COLUMN));
        txtId.setText(map.get(SECOND_COLUMN));
        txtTitle.setText(map.get(THIRD_COLUMN));
        txtBody.setText(map.get(FOURTH_COLUMN));

        return convertView;
    }
}
