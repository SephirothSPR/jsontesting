package com.inoover.www.webapitesting;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import static com.inoover.www.webapitesting.Constants.FIRST_COLUMN;
import static com.inoover.www.webapitesting.Constants.FOURTH_COLUMN;
import static com.inoover.www.webapitesting.Constants.SECOND_COLUMN;
import static com.inoover.www.webapitesting.Constants.THIRD_COLUMN;


public class MainActivity extends AppCompatActivity {

    private ArrayList<HashMap<String, String>> list;
    private String readJSON;
    private TextView textview;
    public String getJSON(String address){
        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(address);
        try{
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if(statusCode == 200){
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while((line = reader.readLine()) != null){
                    builder.append(line);
                }
            } else {
                Log.e(MainActivity.class.toString(), "Failedet JSON object");
            }
        }catch(ClientProtocolException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
        return builder.toString();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView=(ListView)findViewById(R.id.MAINLISTVIEW);
        textview = (TextView)findViewById(R.id.TEST_TEXT);

        list=new ArrayList<HashMap<String, String>>();
//
//        HashMap<String,String> temp=new HashMap<String, String>();
//        temp.put(FIRST_COLUMN, "Ankit Karia");
//        temp.put(SECOND_COLUMN, "Male");
//        temp.put(THIRD_COLUMN, "22");
//        temp.put(FOURTH_COLUMN, "Unmarried");
//        list.add(temp);
//
//        HashMap<String,String> temp2=new HashMap<String, String>();
//        temp2.put(FIRST_COLUMN, "Rajat Ghai");
//        temp2.put(SECOND_COLUMN, "Male");
//        temp2.put(THIRD_COLUMN, "25");
//        temp2.put(FOURTH_COLUMN, "Unmarried");
//        list.add(temp2);
//
//        HashMap<String,String> temp3=new HashMap<String, String>();
//        temp3.put(FIRST_COLUMN, "Karina Kaif");
//        temp3.put(SECOND_COLUMN, "Female");
//        temp3.put(THIRD_COLUMN, "31");
//        temp3.put(FOURTH_COLUMN, "Unmarried");
//        list.add(temp3);

        RetreiveWebApi task = new RetreiveWebApi();
        try{
            readJSON = (String) task.execute().get();
        }catch (Exception e){

        }

        try{
            JSONArray jsonarray = new JSONArray(readJSON);
            for(int i=0; i<jsonarray.length(); i++){
                JSONObject json_data = jsonarray.getJSONObject(i);
                String jUsrId = json_data.getString("userId");
                String jId = json_data.getString("id");
                String jTitle = json_data.getString("title");
                String jBody = json_data.getString("body");

                HashMap<String,String> temp4=new HashMap<String, String>();
                temp4.put(FIRST_COLUMN, jUsrId);
                temp4.put(SECOND_COLUMN, jId);
                temp4.put(THIRD_COLUMN, jTitle);
                temp4.put(FOURTH_COLUMN, jBody);
                list.add(temp4);
                Log.e(MainActivity.class.toString(), jId);
            }
        } catch(Exception e){e.printStackTrace();}


//        textview.setMovementMethod(new ScrollingMovementMethod());
//        textview.setText(readJSON);

        ListViewAdapters adapter=new ListViewAdapters(this, list);
        listView.setAdapter(adapter);

//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
//                int pos = position + 1;
//                Toast.makeText(MainActivity.this, Integer.toString(pos) + " Clicked", Toast.LENGTH_SHORT).show();
//            }
//
//        });

        //String readJSON = getJSON("http://jsonplaceholder.typicode.com/posts");
        //try{
        //    JSONObject jsonObject = new JSONObject(readJSON);
        //    Log.i(MainActivity.class.getName(), jsonObject.getString("date"));
        //} catch(Exception e){e.printStackTrace();}


    }



    private class RetreiveWebApi extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            String tmp;
            tmp = getJSON("http://jsonplaceholder.typicode.com/posts");
            return tmp;
        }
    };


}
