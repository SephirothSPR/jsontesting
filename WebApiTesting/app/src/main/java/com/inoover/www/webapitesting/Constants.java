package com.inoover.www.webapitesting;

/**
 * Created by SephirothSPR on 10/17/2015.
 */
public class Constants {

    public static final String FIRST_COLUMN="First";
    public static final String SECOND_COLUMN="Second";
    public static final String THIRD_COLUMN="Third";
    public static final String FOURTH_COLUMN="Fourth";
}
